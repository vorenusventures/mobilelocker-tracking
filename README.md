This is the Mobile Locker JavaScript SDK.

Read the documentation here:
[https://mobilelocker.readme.io/reference](https://mobilelocker.readme.io/reference)

# Developer Deployment Instructions

* Commit the changes to git and push to BitBucket on the master branch.
* Run this command:

```bash
yarn compile
npm publish
```
